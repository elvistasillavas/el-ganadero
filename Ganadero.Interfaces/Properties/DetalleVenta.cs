﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ganadero.Model.Entidades
{
     public class DetalleVenta
    {
         public Int32 detalleId { get; set;}
         // venta
        public Int32 IdVentas { get; set; }
        public Venta venta { get; set; }
        
               
         //producto
         public Int32 idProducto { get; set; }
        public Producto producto { get; set; }
        public Int32 stockModificar { get; set; }
        public string precio { get; set; }
         // atributos  detalle venta

         public int cantidad { get; set; }
         public double totalPagar { get; set; }

      
    }
}
