﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ganadero.Model.Entidades;

namespace Ganadero.Interfaces.Interfaces
{
    public interface InterfaceServicio
    {
        List<Servicio> All();

        Servicio Find(int id);

        void Store(Servicio servicio);

        void Update(Servicio servicio);

        void Delete(int id);

        List<Servicio> ByQueryAll(string query);
    }
}
