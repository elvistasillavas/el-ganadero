﻿using Ganadero.Model.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ganadero.Interfaces.Interfaces
{
    public interface InterfaceDetalle
    {
        List<DetalleVenta>ByQueryAll(int  id);
    }
}
