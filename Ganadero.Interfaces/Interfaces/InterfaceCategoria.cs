﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 using  Ganadero.Model.Entidades;

namespace Ganadero.Interfaces.Interfaces
{
     public interface InterfaceCategoria
    {
         Categoria Find(int id);
         List<Categoria> All();
         List<Categoria> ByQueryAll(string query ,string i,string s);
    }
}
