﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ganadero.Model.Entidades;

namespace Ganadero.Interfaces.Interfaces
{
    public interface InterfaceDevolucion
    {
        List<ComprobanteDeVenta> All();

        ComprobanteDeVenta Find(int id);
        void Store(ComprobanteDeVenta producto);
        void Update(ComprobanteDeVenta producto);
        void Delete(int id);
        List<ComprobanteDeVenta> ByQueryAll(string query);
    }
}
