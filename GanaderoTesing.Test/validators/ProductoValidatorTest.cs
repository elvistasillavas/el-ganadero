﻿using Ganadero.Model.Entidades;
using Validators.validatorsEntities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GanaderoTesing.Test.validators
{
    [TestFixture]
    class ProductoValidatorTest
    {
        [Test]
        public void ProductoSinNombreDebeRetornarFalse()
        {
            var validator = new ProductoValidator();
            var producto = new Producto();

            var result = validator.Pass(producto);

            Assert.False(result);
        }

        [Test]
        public void ProductoConNombreYCodigoDebeRetornarTrue()
        {
            var validator = new ProductoValidator();
            var producto = new Producto
            {
                Marca="vaas",

            };

            var result = validator.Pass(producto);

            Assert.True(result);

        }
    }
}
