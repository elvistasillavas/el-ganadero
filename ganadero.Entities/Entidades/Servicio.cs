﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElGanadero.Models
{
    public class Servicio
    {
        public Guid Id_servicio { get; set; }
        public decimal Precio { get; set; }
        public string NombreServicio { get; set; }
        public string Descripcion { get; set; }
    }
}