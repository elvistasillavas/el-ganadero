﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElGanadero.Models
{
    public class ComprobanteDeVenta
    {
        public string NombreCliente { get; set; }
        public string DNIoRUC { get; set; }
        public DateTime fecha { get; set; }
        public Int32 NumeroDeComprobante { get; set; }
        public string NombreProducto { get; set; }
        public Int32 Id_Producto { get; set; }
        public decimal TotalPagar { get; set; }

        public Producto Producto { get; set; }
        public Servicio Servicio { get; set; }
        public Venta Venta { get; set; }
    }
}