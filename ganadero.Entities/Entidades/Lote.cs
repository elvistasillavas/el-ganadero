﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElGanadero.Models
{
    public class Lote
    {
        public Guid IdLote { get; set; }
        public DateTime FechaDeVencimiento { get; set; }
        public Int32 NroLote { get; set; }
        public Int32 StockInicial { get; set; }
    }
}