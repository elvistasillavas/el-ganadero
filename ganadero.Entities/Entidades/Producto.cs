﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElGanadero.Models
{
    public class Producto
    {
        public Guid IdProducto { get; set; }
        public string NombreProducto { get; set; }

        public decimal Precio { get; set; }
        public string Marca { get; set; }
        public Guid IdCategoria { get; set; }
        public Categoria Categoria { get; set; }
        public Stock Stock { get; set; }
    }
}