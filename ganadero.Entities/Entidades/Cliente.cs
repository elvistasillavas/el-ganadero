﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElGanadero.Models
{
    public class Cliente
    {
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string DNIoRUC { get; set; }
        public string Apellidos { get; set; }
        public string Telefonos { get; set; }
        public Guid IdCliente { get; set; }
    }
}