﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElGanadero.Models
{
    public class Stock
    {
        public Guid Idstock { get; set; }
        public Int32 IdLote { get; set; }
        public Int32 Stock_Actual { get; set; }

        public Lote Lote { get; set; }
    }
}