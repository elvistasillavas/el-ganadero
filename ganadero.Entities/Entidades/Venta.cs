﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElGanadero.Models
{
    public class Venta
    {

        public Guid IdVentas { get; set; }
        public Int32 IdUsuario { get; set; }
        public Int32 IdCliente { get; set; }
        public Cliente Cliente { get; set; }

        public Usuario Usuario { get; set; }
    
    }
}