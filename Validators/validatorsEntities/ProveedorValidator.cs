﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ganadero.Model.Entidades;

namespace Validators.validatorsEntities
{
    public class ProveedorValidator
    {
        public virtual bool Pass(Proveedor proveedor)
        {
            if (String.IsNullOrEmpty(proveedor.Nombre))
                return false;
            //if (servicio.Id_servicio == Int32.Parse(""))
            //    return false;
            //if (servicio.Precio == decimal.Parse(""))
            //    return false;
            if (String.IsNullOrEmpty(proveedor.Ruc))
                return false;
            return true;
        }
    }
}
