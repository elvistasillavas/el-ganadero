﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ganadero.Model.Entidades;

namespace Validators.validatorsEntities
{
   public  class DevolucionValidator
    {

       public virtual bool Pass(ComprobanteDeVenta compra)
            {
                if (String.IsNullOrEmpty(compra.NombreCliente))
                    return false;
                if (String.IsNullOrEmpty(compra.NombreProducto))
                    return false;
                return true;
            }
        }
    
}
