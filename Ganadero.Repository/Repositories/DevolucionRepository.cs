﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ganadero.Interfaces.Interfaces;
using Ganadero.Model.Entidades;
using GenerateDB;

namespace Ganadero.Repository.Repositories
{
  public  class DevolucionRepository:InterfaceDevolucion
    {

      DBSystemContext entities;
     
        public DevolucionRepository(DBSystemContext entities)
        {
            this.entities = entities;
        }

       

        public List<ComprobanteDeVenta> All()
        {
            var result = from p in entities.ComprobateDeVentas select p;
            return result.ToList();
        }

      

        public ComprobanteDeVenta Find(int id)
        {
            var result = from p in entities.ComprobateDeVentas where p.idProducto == id select p;
            return result.FirstOrDefault();
        }


       
        public void Store(ComprobanteDeVenta producto)
        {
            entities.ComprobateDeVentas.Add(producto);
            entities.SaveChanges();
        }


       
        public void Update(ComprobanteDeVenta producto)
        {
            var result = (from p in entities.ComprobateDeVentas where p.idProducto == producto.idProducto select p).First();

            result.NombreProducto = producto.NombreProducto;
            result.DNIoRUC = producto.DNIoRUC;
            result.NombreCliente = producto.NombreCliente;

            result.TotalPagar= producto.TotalPagar;
            result.fecha = producto.fecha;



            entities.SaveChanges();
        }

     
        public void Delete(int id)
        {
            var result = (from p in entities.ComprobateDeVentas where p.idProducto == id select p).First();
            entities.ComprobateDeVentas.Remove(result);
            entities.SaveChanges();
        }

      
       public List<ComprobanteDeVenta> ByQueryAll(string query)
        {
            var dbQuery = (from p in entities.ComprobateDeVentas select p);

            if (!String.IsNullOrEmpty(query))
                dbQuery = dbQuery.Where(o => o.NombreProducto.Contains(query));
            return dbQuery.ToList();
        }
    }
}
