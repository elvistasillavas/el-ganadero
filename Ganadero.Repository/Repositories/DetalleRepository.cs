﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ganadero.Model.Entidades;
using Ganadero.Interfaces.Interfaces;
using GenerateDB;
namespace Ganadero.Repository.Repositories
{
    public class DetalleRepository:InterfaceDetalle
    {
          DBSystemContext entities;

          public DetalleRepository(DBSystemContext entities)
        {
            this.entities = entities;
        }



          public List<DetalleVenta> ByQueryAll(int id)
          {
              var dbQuery = (from p in entities.DetalleVenta select p);

              if (id != null)
                  dbQuery = dbQuery.Where(o => o.detalleId == id);
              return dbQuery.ToList();
          }
    }
}
