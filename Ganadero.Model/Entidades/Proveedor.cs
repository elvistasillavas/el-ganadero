﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ganadero.Model.Entidades
{
    public class Proveedor
    {
       [Required]
        public int IdProveedor { get; set; }
       [Required]
        public string Ruc { get; set; }
        [Required]
        public string Nombre { get; set; }
       [Required]
      // [Range(8, 10, ErrorMessage = "Ingrese un número celular correcto")]
        public string Telefono { get; set; }
       [Required]
        public string Ciudad { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "El email no tiene el formato correcto")]
        public string Correo { get; set; }
      [Required]
        public string PaginaWeb { get; set; }

  
    }
}
