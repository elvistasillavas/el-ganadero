﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ganadero.Model.Entidades
{
    public class Servicio
    {
        [Required]
        public Int32 Id_servicio { get; set; }
        [Required]
        public decimal Precio { get; set; }
        [Required]
        public string NombreServicio { get; set; }
        [Required]
        public string Descripcion { get; set; }
        
    }
}