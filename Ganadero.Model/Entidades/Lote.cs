﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ganadero.Model.Entidades
{
    public class Lote
    {
        public Int32 IdLote { get; set; }


        [Required(ErrorMessage = "Hey, este dato es fundamental")]
        [DisplayName("Fecha de nacimiento")]
        public DateTime FechaDeVencimiento { get; set; }
        public Int32 NroLote { get; set; }
        public Int32 StockInicial { get; set; }
        public int idProducto { get; set; }
        public Producto producto { get; set; }

        //proveedor
        public int idProveedor { get; set; }
        public Proveedor Proveedor{ get; set; }
    }
}