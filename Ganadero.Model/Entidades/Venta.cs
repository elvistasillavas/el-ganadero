﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ganadero.Model.Entidades
{
    public class Venta
    {
        public Int32 ventaID { get; set; }
        public string tipocomprobante { get; set; }
        public DateTime fecha { get; set; }
        public double total { get; set; }

        public Int32 IdVentas { get; set; }
        public Int32 IdUsuario { get; set; }
        public Int32 IdCliente { get; set; }
        public Cliente Cliente { get; set; }

        public Usuario Usuario { get; set; }
    
        public  virtual List<DetalleVenta>detalleventa { get; set; }  
        public List<Producto>productos { get; set; }  
    }
}