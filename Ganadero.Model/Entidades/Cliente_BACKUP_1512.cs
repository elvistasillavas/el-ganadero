﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ganadero.Model.Entidades
{
    public class Cliente
    {
        public string Nombre { get; set; }
        
        public Int32 DNI { get; set; }
        public Int32 RUC { get; set; }
        public string Apellidos { get; set; }
        public string Telefonos { get; set; }
        public Int32 IdCliente { get; set; }

        public virtual List<Venta> venta { get; set; }
    }
}