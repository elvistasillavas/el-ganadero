﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElGanadero.Models
{
    public class Producto
    {
        public string Nommbre { get; set; }
        public int Codigo { get; set; }
        public Double Precio { get; set; }
        public string Marca { get; set; }

        public Int32 IdCategoria { get; set; }
        public Categoria Categoria { get; set; }
    }
}