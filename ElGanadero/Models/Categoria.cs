﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElGanadero.Models
{
    public class Categoria
    {
        public Int32 IdCategoria { get; set; }
        public string Descripcion { get; set; }
    }
}