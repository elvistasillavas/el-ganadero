﻿using Ganadero.Interfaces.Interfaces;
using Ganadero.Model.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Validators.validatorsEntities;

namespace ElGanadero.Controllers
{
    public class LoteController : Controller
    {
        private InterfaceLote repository;
        private LoteValidator validator;
        private InterfaceProducto productorepository;
        private InterfaceProveedor proveedorrepository;

        public LoteController(InterfaceLote repository, LoteValidator validator, InterfaceProducto productorepository, InterfaceProveedor proveedorrepository)
        {
            this.repository = repository;
            this.validator = validator;
            this.productorepository = productorepository;
            this.proveedorrepository = proveedorrepository;
        }


        [HttpGet]
        public ViewResult Inicio(DateTime? date1,DateTime? date2)
        {
            var dato = repository.ByQueryAll( date1,date2);
            return View("Inicio",dato);
        }


        [HttpGet]
        public ViewResult Create()
        {
            var proveedor = proveedorrepository.Allnombre();
            ViewData["idProveedor"] = new SelectList(proveedor, "IdProveedor","Nombre");

            var producto = productorepository.Allnombre();
            ViewData["idProducto"] = new SelectList(producto, "IdProducto", "NombreProducto");

            return View("Create");
        }

        [HttpPost]
        public ActionResult Create(Lote lote)
        {

            if (validator.Pass(lote))
            {
                repository.Store(lote);

                TempData["UpdateSuccess"] = " el producto se a guardado correctamente";
                return RedirectToAction("Inicio");
            }

            return View("Create", lote);
        }

        [HttpGet]
        public ViewResult Editar(int id)
        {
            var data = repository.Find(id);
            var proveedor = proveedorrepository.Allnombre();
            ViewData["idProveedor"] = new SelectList(proveedor, "IdProveedor", "Nombre",data.idProveedor);

            var producto = productorepository.Allnombre();
            ViewData["idProducto"] = new SelectList(producto, "IdProducto", "NombreProducto",data.idProducto);
            return View("Editar", data);
        }

        [HttpPost]
        public ActionResult Editar(Lote lote)
        {
            repository.Update(lote);
            TempData["UpdateSuccess"] = "Se Actualizó Correctamente";
            return RedirectToAction("Inicio");
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            repository.Delete(id);
            TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
            return RedirectToAction("Inicio");
        }
    }
}
