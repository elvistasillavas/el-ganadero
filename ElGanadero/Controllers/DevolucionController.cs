﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Ganadero.Interfaces.Interfaces;
using Ganadero.Model.Entidades;
using Validators.validatorsEntities;

namespace ElGanadero.Controllers
{
    public class DevolucionController
    {
         private InterfaceDevolucion repository;
        private  DevolucionValidator validators;
    
        public DevolucionController(InterfaceDevolucion repository, DevolucionValidator validator)
        {
            this.repository = repository;
            this.validators = validator;
        }

        [HttpGet]
        public ViewResult Index(string query = "")
        {
            var datos = repository.ByQueryAll(query);
            return View("Inicio", datos);
        }

        //[HttpGet]
        //public ViewResult Create()
        //{
        //    return View("Create");
        //}

        //[HttpPost]
        //public ActionResult Create(ComprobanteDeVenta devolucion)
        //{

        //    if (validator.Pass(devolucion))
        //    {
        //        repository.Store(devolucion);

        //        TempData ["UpdateSuccess"] = "  se a guardado correctamente";
        //        return RedirectToAction("Inicio");
        //    }

        //    return View("Create", devolucion);
        //}


        //[HttpGet]
        //public ViewResult Editar(int id)
        //{
        //    var data = repository.Find(id);

        //    return View("Editar", data);
        //}

        //[HttpPost]
        //public ActionResult Editar(ComprobanteDeVenta devolucion)
        //{
        //    repository.Update(devolucion);
            
        //    TempData["UpdateSuccess"] = "Se Actualizó Correctamente";
        //    return RedirectToAction("Inicio");
        //}

        //[HttpGet]
        //public ActionResult Delete(int id)
        //{
        //    repository.Delete(id);
        //    TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
        //    return RedirectToAction("Inicio");
        //}
    }
}