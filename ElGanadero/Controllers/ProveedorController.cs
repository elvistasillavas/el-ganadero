﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ganadero.Interfaces.Interfaces;
using Ganadero.Model.Entidades;
using Validators.validatorsEntities;

namespace ElGanadero.Controllers
{
    public class ProveedorController : Controller
    {
        private InterfaceProveedor repository;
        private ProveedorValidator validator;

        public ProveedorController(InterfaceProveedor repository, ProveedorValidator validator)
        {
            this.repository = repository;
            this.validator = validator;
        }

        [HttpGet]
        public ViewResult Index(string query = "")
        {
            var datos = repository.ByQueryAll(query);
            return View("Index", datos);
        }


        [HttpGet]
        public ViewResult Create()
        {
            return View("Create");
        }

        [HttpPost]
        public ActionResult Create(Proveedor proveedor)
        {

            if (validator.Pass(proveedor))
            {
                repository.Store(proveedor);

                TempData["UpdateSuccess"] = "Se Guardo Correctamente";

                return RedirectToAction("Index");
            }

            return View("Create", proveedor);
        }

        [HttpGet]
        public ViewResult Edit(int id)
        {
            var data = repository.Find(id);
            return View("Edit", data);
        }

        [HttpPost]
        public ActionResult Edit(Proveedor proveedor)
        {
            repository.Update(proveedor);
            TempData["UpdateSuccess"] = "Se Actualizó Correctamente";
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            repository.Delete(id);
            TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
            return RedirectToAction("Index");
        }

    }
}
