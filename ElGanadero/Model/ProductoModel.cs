﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ElGanadero.Model
{
    public class ProductoModel
    {
        [DisplayName("categoria")]
        public int Idcategoria { get; set; }

        public SelectList categoriaList { get; set; }
    }
}